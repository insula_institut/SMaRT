# SMaRT: Sensation Mapping and Reporting Tool

## Overview

Welcome to the official repository for the Sensation Mapping and Reporting Tool, abbreviated as SMaRT. This specialized scientific research software is designed to offer a comprehensive and innovative platform for mapping and documenting human sensations.

You can access the code of the different versions of SMaRT in separate branches.

> **Note**: This application was previously known as "SensationMapper" and "SymptomMapper" in earlier studies.

## What is SMaRT?

SMaRT is a scientific app for Android-based tablet PCs. It aims to provide objective documentation of bodily sensations like tingling, fullness, warmth, etc., using a digital, annotatable body outline.

Users can mark the quality, location, extent, and intensity of sensations on this body outline. All of this information is stored locally on the mobile device and can be exported by the user at their discretion.

## Scientific Applications

SMaRT's precursor, named "SymptomMapper," was validated in the following publication:

- Neubert T, et al. "Designing a Tablet-Based Software App for Mapping Bodily Symptoms: Usability Evaluation and Reproducibility Analysis." Journal of Medical Internet Research Mhealth Uhealth. 2018;6(5):e127. DOI:10.2196/mhealth.8409

Several versions of the app have been employed in various scientific contexts, as demonstrated in the following publications:

- Manuel J, et al. "Traumatic events, post-traumatic stress disorder, and central sensitization in chronic pain patients." Psychosomatic Medicine, 2023, 85(4), 351. DOI: 10.1097/PSY.0000000000001181

- Shaballout N, et al. "Lateralization and Bodily Patterns of Segmental Signs and Spontaneous Pain in Acute Visceral Disease: Observational Study." Journal of Medical Internet Research, 2021;23(8):e27247. DOI: 10.2196/27247

- Shaballout N, et al. "Digital Pain Drawings Can Improve Doctors’ Understanding of Acute Pain Patients: Survey and Pain Drawing Analysis." Journal of Medical Internet Research Mhealth Uhealth 2019;7(1):e11412. DOI:10.2196/11412

- And more...

## Hannover Body Template

SMaRT uses the Hannover Body Template for sensation drawings. This free set of body outlines is designed to cover as much body surface as possible in four different views.

The template is based on a photograph of a real person and is available in female, male, and gender-neutral versions.

## Copyright and License

### Copyright

The current version of SMaRT was developed by the Insula-Institute (Florian Beissner) in cooperation with the University of Bielefeld (Evgenii Pustozerov). Previous versions were developed since 2014 at the Hannover Medical School by Tawfik Moher Alsady, Florian Beissner, Till-Ansgar Neubert, and Jorge Manuel Sánchez.

### License

The source code of SMaRT is licensed under the GNU General Public License Version 3 (GPLv3). This means you can freely use, modify, and distribute the software, provided you meet the terms of the GPLv3, including the obligation to release modified versions under the same license.

## Privacy and Data Protection

The use of SMaRT does not require any personal data. However, upon launching the app, you are asked for a name or pseudonym. Drawings and associated information are stored locally on your device. No data transfer to external servers or third parties occurs. Access to this data is protected by a password set by you.

## Disclaimer

SMaRT is not a medical product and should not be used for medical diagnostic or therapeutic purposes.

## Support Our Project!

If you find SMaRT useful and want to support our work, we would be grateful for donations. Your financial support helps us continue to develop the app and offer it free of charge to all users.

[Donate Now](https://spenden.twingle.de/insula-institut-fur-integrative-therapieforschung/allgemein-spenden/tw601042f7a6d1f/page)

## Download Compiled App

SMaRT is available as a compiled version for download. You can download the APK file from the [Insula-Institut Software Page](https://www.insula-institut.org/software). The file can be installed on your mobile device once downloaded.

---

For any issues or questions, please [contact us](mailto:smart@insula-institut.org).
